package com.bigbankpro;
import com.bigbankpro.model.Account;
import com.bigbankpro.model.CreditCard;
import com.bigbankpro.service.AccountService;
import com.bigbankpro.service.CreditCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * Created by Padre Loco on 11/11/2019.
 * Copyright (c) 2019 All rights reserved.
 */


@SpringBootApplication
public class BbprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(BbprojectApplication.class, args);
	}

	private static boolean populate = false; //set to true to create dummy accounts and credit cards

	@Autowired
	private AccountService accountService;

	@Autowired
	private CreditCardService creditCardService;

	@Bean
	public CommandLineRunner populateDatabase() {
		return new DatabasePopulatorRunner();
	}

	public class DatabasePopulatorRunner implements CommandLineRunner {

		Account account = new Account("CC07800654", 234.78f);
		Account account2 = new Account("CC78000089", 67f);

		CreditCard creditCard = new CreditCard("EXS896", account);
		CreditCard creditCard2 = new CreditCard("VEH897", account2);
		CreditCard creditCard3= new CreditCard("EX46TS", account);


		@Override
		public void run(String... args) throws Exception {
			//Order: accounts then credit card!!

			if (populate)
				populateItLikeALoco();

		}


		private void populateItLikeALoco() throws Exception {
			synchronized (this) {
				accountService.saveAccount(account);
				accountService.saveAccount(account2);
				accountService.saveCreditCard(creditCard);
				accountService.saveCreditCard(creditCard2);
				accountService.saveCreditCard(creditCard3);
			}
		}
	}
}
