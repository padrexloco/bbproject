package com.bigbankpro.utils;
import com.bigbankpro.exception.NoCoherentRequestException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Calendar;

/**
 * Created by Padre Loco on 13/11/2019.
 * Copyright (c) 2019 All rights reserved.
 */

public class ValidationUtils {

    public static DateTime convertStringDateToDateTime(String stringDate){
        String datePattern = "yyyy-MM-dd";
        DateTime dateTime; // = new DateTime();
        DateTimeFormatter formatter = DateTimeFormat.forPattern(datePattern);
        dateTime = formatter.parseDateTime(stringDate);
        return dateTime;
    }

    public static void checkDateValidity(int year, int month, int day){
        //not future date
        String stringDate = String.valueOf(year) + "-" + String.valueOf(month) + "-" + String.valueOf(day);
        if ((convertStringDateToDateTime(stringDate).isAfterNow()) ||
                (day < 0 || day > 31) ||
                (month < 1 || month > 12) ||
                (year < 1970) ||
                ((day == 30 || day == 31) && month == 2))
            throw new NoCoherentRequestException("Wrong transaction date: "+ stringDate);
    }

    public static  void checkAccountNumber(String accNo){
        final int ACC_NO_LEN = 10;
        if (!(accNo != null && accNo.matches("^[a-zA-Z0-9]*$")) && accNo.length() == ACC_NO_LEN)
            throw new NoCoherentRequestException("The account number ", accNo, ". Try again!");
    }

    public static void checkCreditCardNumber(String cCNo){
        final int CC_NO_LEN = 6;
        if (!(cCNo != null && cCNo.matches("^[a-zA-Z0-9]*$")) && cCNo.length() == CC_NO_LEN)
            throw new NoCoherentRequestException("The credit card number ", cCNo, ". Try again!");
    }

    public static void checkMonth(int month){
        if (month < 1 || month > 12)
            throw new NoCoherentRequestException("The month ", String.valueOf(month), ". Choose a number between 1 and 12.");
    }

    public static void checkYear(int year){
        if (year < 1970 || (year > Calendar.getInstance().get(Calendar.YEAR)))
            throw new NoCoherentRequestException("The year ", String.valueOf(year), ". Choose a number between 2000 " + String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));
    }

    public static void checkSign(String sign){
        if (!(sign != null && (sign.equalsIgnoreCase("cr") || sign.equalsIgnoreCase("dr"))))
            throw new NoCoherentRequestException("The accounting entry ", sign, ". Choose between 'DR' and 'CR'." );
    }

    public static void checkAmount(float amount){
        if (amount <= 0) throw new NoCoherentRequestException("The amount provided " + amount  + " is incoherent! \n Please provide the correct amount.");
    }

    static void checkAccountMovementText(String accMvtTxt){
        final int TEXT_LEN = 30;
        if (!(accMvtTxt != null &&
                accMvtTxt.length() == TEXT_LEN &&
                accMvtTxt.matches("^[a-zA-Z0-9]*$")))
            throw new NoCoherentRequestException("The account movement entry '" + accMvtTxt +  "' is corrupted." );
    }
}
