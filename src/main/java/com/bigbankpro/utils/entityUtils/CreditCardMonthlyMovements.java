package com.bigbankpro.utils.entityUtils;


import com.bigbankpro.model.CreditCardMovement;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Padre Loco on 14/11/2019.
 * Copyright (c) 2019 All rights reserved.
 */


public class CreditCardMonthlyMovements {

    private List<CreditCardMovement> creditCardMovements = new ArrayList<>();
    private float totalCreditCardAmount;

    public CreditCardMonthlyMovements(List<CreditCardMovement> creditCardMovements, float totalCreditCardAmount) {
        this.creditCardMovements = creditCardMovements;
        this.totalCreditCardAmount = totalCreditCardAmount;
    }

    public List<CreditCardMovement> getCreditCardMovements() {
        return creditCardMovements;
    }

    public void setCreditCardMovements(List<CreditCardMovement> creditCardMovements) {
        this.creditCardMovements = creditCardMovements;
    }

    public float getTotalCreditCardAmount() {
        return totalCreditCardAmount;
    }

    public void setTotalCreditCardAmount(float totalCreditCardAmount) {
        this.totalCreditCardAmount = totalCreditCardAmount;
    }
}
