package com.bigbankpro.utils.entityUtils;

import com.bigbankpro.model.Account;
import com.bigbankpro.model.CreditCard;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Padre Loco on 14/11/2019.
 * Copyright (c) 2019 All rights reserved.
 */

public class AccountCreditCardUtils {

    private List<Account> accounts = new ArrayList<>();
    private List<CreditCard> creditCards = new ArrayList<>();

    public AccountCreditCardUtils(List<Account> accounts, List<CreditCard> creditCards) {
        this.accounts = accounts;
        this.creditCards = creditCards;
    }

    public AccountCreditCardUtils() {
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public List<CreditCard> getCreditCards() {
        return creditCards;
    }

    public void setCreditCards(List<CreditCard> creditCards) {
        this.creditCards = creditCards;
    }
}
