package com.bigbankpro.utils;

/**
 * Created by Padre Loco on 11/11/2019.
 * Copyright (c) 2019 All rights reserved.
 */


public class NotificationMessage {

    private static String AUTO_NOTIF_MESSAGE = "* THIS IS AN AUTOMATIC NOTIFICATION! *";

    private static String getNotificationAboutSuccessFileUpload(String fileName){
        return "The file <<" + fileName + ">> has been uploaded successfully!";
    }

    private static String getNotificationAboutFileCorruption(String fileName){
        return "Failure in uploading the file <<" + fileName + ">>. The file is corrupted!";
    }

    private static String getNotificationAboutWrongFileType(String fileName){
        return "Failure in uploading the file <<" + fileName + ">>. Select either a '.csv', '.txt' or '.xml' file type!";
    }

    private static String getNotificationAboutNullActivity(String element){
        return " has no activity in the selected period";
    }

}

