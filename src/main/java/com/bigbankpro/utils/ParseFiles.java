package com.bigbankpro.utils;

import com.bigbankpro.exception.NoCoherentRequestException;
import com.bigbankpro.model.Account;
import com.bigbankpro.model.CreditCard;
import com.bigbankpro.service.AccountService;
import com.bigbankpro.utils.entityUtils.AccountActivityUtils;
import com.bigbankpro.utils.entityUtils.AccountCreditCardUtils;
import com.bigbankpro.utils.entityUtils.CreditCardMovementUtils;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by Padre Loco on 13/11/2019.
 * Copyright (c) 2019 All rights reserved.
 */


public class ParseFiles {

    @Autowired
    private static AccountService accountService;

    private static final CsvMapper mapper = new CsvMapper();

    public static <T> List<T> readCsv(Class<T> clazz, InputStream stream) throws IOException {
        CsvSchema schema = mapper.schemaFor(clazz).withHeader().withColumnReordering(true);
        ObjectReader reader = mapper.readerFor(clazz).with(schema);
        return reader.<T>readValues(stream).readAll();
    }

    public static List<AccountActivityUtils> getAccActivitiesFromTxt(File txtFile){
        String fileName = txtFile.getName();
        List<AccountActivityUtils> accountActivityUtilsList = new ArrayList<>();

        try (Stream<String> stream = Files.lines(txtFile.toPath())){
            stream.forEach(entry ->{
                //Parse and validate String
                ValidationUtils.checkAccountMovementText(entry);
                String accNo = entry.substring(0, 10);
                accNo = accNo.toUpperCase();
                int year = Integer.parseInt(entry.substring(10, 14));
                int month = Integer.parseInt(entry.substring(14, 16));
                int day = Integer.parseInt(entry.substring(16, 18));
                ValidationUtils.checkDateValidity(year,month,day);
                int whole = Integer.parseInt(entry.substring(18, 24));
                String decimal = entry.substring(24, 28);
                float activityAmount = (float) whole + parseStringDecimal(decimal);
                ValidationUtils.checkAmount(activityAmount);
                String sign = entry.substring(28, 30);
                sign = sign.toUpperCase();
                ValidationUtils.checkSign(sign);

                AccountActivityUtils accountActivityUtils = new AccountActivityUtils(
                        accNo, day, month, year, activityAmount, sign, entry.toUpperCase());
                accountActivityUtilsList.add(accountActivityUtils);
            });
            return accountActivityUtilsList;
        } catch (IOException e) {
            throw new NoCoherentRequestException("Error encountered while uploading * " + fileName + " * : " + e.getMessage());
        }
    }

    public static float parseStringDecimal(String decimal){
        int lenDecimal = decimal.length();
        int decim = Integer.parseInt(decimal);

        int powerTen = 1;
        for (int i = 0; i < lenDecimal; i++) {
            powerTen *= 10;
        }
        return (float) decim / powerTen;
    }

    public static List<CreditCardMovementUtils> getCCMovementsFromCsv(File csvFile){

        List<CreditCardMovementUtils> cCMovementList = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            String line;
            int counter = 0;
            while ((line = br.readLine()) != null) {
                // process the line except the first one
                if (counter > 0) {                          //jump the first line!

                    String[] data = line.split(",");
                    String cCNo = data[0];
                    String date = data[1];
                    String amount = data[2];

                    cCNo = cCNo.toUpperCase();

                    int day = Integer.parseInt(date.split("/")[0]);
                    int month = Integer.parseInt(date.split("/")[1]);
                    int year = Integer.parseInt(date.split("/")[2]);
                    float debitAmount;
                    if (amount.contains(".")) {
                        String amountWhole = amount.split("\\.")[0];
                        String amountDecimal = amount.split("\\.")[1];
                        int powerTen = 1;
                        for (int i = 0; i < amountDecimal.length(); i++) {
                            powerTen *= 10;
                        }
                        debitAmount = Float.parseFloat(amountWhole) + (Float.parseFloat(amountDecimal) / powerTen);
                    }
                    debitAmount = Float.parseFloat(amount);

                    CreditCardMovementUtils cCMovement = new CreditCardMovementUtils();
                    cCMovement.setcCNo(cCNo);
                    cCMovement.setYear(year);
                    cCMovement.setMonth(month);
                    cCMovement.setDay(day);
                    cCMovement.setAmount(debitAmount);

                    cCMovementList.add(cCMovement);
                }
                counter++;
            }
        } catch(IOException e){
            throw new NoCoherentRequestException("Error: " + e.getMessage());
        }
        return cCMovementList;
    }

    public static AccountCreditCardUtils getAccountsAndCreditCardsFromXml(File xmlFile){
        List<Account> accounts = new ArrayList<>();
        List<CreditCard> creditCards = new ArrayList<>();

        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(xmlFile);
            document.getDocumentElement().normalize();

            String tagName = "conto";
            NodeList nodeList = document.getElementsByTagName(tagName);

            for (int accIndex = 0; accIndex < nodeList.getLength(); accIndex++ ){

                Node node = nodeList.item(accIndex);

                System.out.println("\nCurrent Element: " + node.getNodeName());

                if (node.getNodeType() == Node.ELEMENT_NODE){
                    Element element = (Element) node;
                    String accNo = element.getAttribute("Numero").toUpperCase();


                    //controll the account (length and existence)
                    if (!(accNo.length() == 10)){
                        String message = "Account number read: " + accNo +
                                " is not valid!\nLength allowed: 10 characters!";
                        System.out.println(message);
                        break;
                    }

                    Account account = new Account(accNo);
                    accounts.add(account);


                    //loop for carte di credito del conto (node) [getElementsByTagName("carta").length()]
                    for (int cCIndex = 0; cCIndex < element.getElementsByTagName("carta").getLength(); cCIndex++) {
                        String cCNo = element.getElementsByTagName("carta").item(cCIndex).getAttributes().item(0).toString();// parse after " or sunstring(8,14)
                        cCNo = cCNo.substring(8,14);

                        CreditCard creditCard = new CreditCard(cCNo, account);
                        creditCards.add(creditCard);
                        System.out.println("Carta Numero: " + cCNo);
                    }
                }
            }

        }catch (Exception e){
            throw new NoCoherentRequestException("Fatal Error: " + e.getMessage());
        }

        //Return an object that contains 2 arrays of objects!
        return new AccountCreditCardUtils(accounts, creditCards);

    }

    private static boolean ifAccountHasLengthTen(String accNo){
        return accNo.length() == 10;
    }

    private static boolean newAccount(String accNo){
        return accountService.checkWhetherAccountExists(accNo);
    }

}
