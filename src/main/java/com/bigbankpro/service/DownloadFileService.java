package com.bigbankpro.service;

import com.bigbankpro.exception.NoCoherentRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Service
public class DownloadFileService {

    private final AccountService accountService;

    private final AccountActivityService accountActivityService;

    private final CreditCardMovementService creditCardMovementService;

    @Autowired
    public DownloadFileService(AccountService accountService, AccountActivityService accountActivityService, CreditCardMovementService creditCardMovementService) {
        this.accountService = accountService;
        this.accountActivityService = accountActivityService;
        this.creditCardMovementService = creditCardMovementService;
    }

    public String uploadFile(MultipartFile multipartFile) throws IOException {

        if (multipartFile.isEmpty()) {
            throw new NoCoherentRequestException("Failed to store empty file");
        }

        String fileName = multipartFile.getOriginalFilename();
        File file = multipartToFile(multipartFile, fileName);
        String fileExtension = fileName.split("\\.")[1].toLowerCase();

        switch (fileExtension){


            case "csv":
                this.creditCardMovementService.uploadCreditCardMovements(file);
                break;

            case "txt":
                this.accountActivityService.uploadAccountMovements(file);
                break;

            case "xml":
                this.accountService.uploadAccountAndAssociatedCreditCard(file);
                break;

            default:
                throw new NoCoherentRequestException("Error! The file type (" + fileExtension.toLowerCase() +
                        ") is not supported!");
        }
        // delete file
        if (file.delete())
            System.out.println("File deleted");
        else
            System.out.println("File was not deleted");
        return "Successfully uploaded the file '" + fileName + "'.";
    }

    private File multipartToFile(MultipartFile multipart, String fileName) throws IllegalStateException, IOException {
        File convFile = new File(System.getProperty("java.io.tmpdir")+"/"+fileName);
        multipart.transferTo(convFile);
        return convFile;
    }
}
