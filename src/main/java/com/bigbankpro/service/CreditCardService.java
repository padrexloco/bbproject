package com.bigbankpro.service;

import com.bigbankpro.model.Account;
import com.bigbankpro.model.CreditCard;
import com.bigbankpro.exception.NoCoherentRequestException;
import com.bigbankpro.exception.NoSuchElementIdException;
import com.bigbankpro.repository.CreditCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by Padre Loco on 12/11/2019.
 * Copyright (c) 2019 All rights reserved.
 */

@Service
public class CreditCardService {

    @Autowired
    private CreditCardRepository creditCardRepository;

    @Autowired
    private AccountService accountService;


    /**
     * *** GETTERS ******
     */


    CreditCard getCreditCardByCcNo(String ccNo){
        ccNo = ccNo.toUpperCase();
        Optional<CreditCard> optionalCreditCard = this.creditCardRepository.findByCcNo(ccNo);
        if (!optionalCreditCard.isPresent())throw new NoSuchElementIdException("No credit card ", ccNo);
        return optionalCreditCard.get();
    }

    public List<CreditCard> getAllCreditCardsOfAssociatedAccount(String accNo){
        Account account = this.accountService.getAccountByAccNo(accNo);
        return this.creditCardRepository.findByAccount(account);
    }


    /**
     * **** MODIFIERS *****
     */

    void saveACreditCard(CreditCard creditCard){
        this.creditCardRepository.save(creditCard);
    }

    void deleteCreditCard(CreditCard creditCard){
        this.creditCardRepository.delete(creditCard);
    }

}
