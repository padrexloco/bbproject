package com.bigbankpro.service;

import com.bigbankpro.model.Account;
import com.bigbankpro.model.CreditCard;
import com.bigbankpro.model.CreditCardMovement;
import com.bigbankpro.repository.CreditCardMovementRepository;
import com.bigbankpro.utils.entityUtils.CreditCardMonthlyMovements;
import com.bigbankpro.utils.entityUtils.CreditCardMovementUtils;
import com.bigbankpro.utils.ParseFiles;
import com.bigbankpro.utils.ValidationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Padre Loco on 12/11/2019.
 * Copyright (c) 2019 All rights reserved.
 */

@Service
public class CreditCardMovementService {

    private final CreditCardMovementRepository creditCardMovementRepository;

    private final CreditCardService creditCardService;

    private final AccountService accountService;

    @Autowired
    public CreditCardMovementService(CreditCardMovementRepository creditCardMovementRepository, CreditCardService creditCardService, AccountService accountService) {
        this.creditCardMovementRepository = creditCardMovementRepository;
        this.creditCardService = creditCardService;
        this.accountService = accountService;
    }


    /**
     * ***** GETTERS ****
     */

    public List<CreditCardMovement> getAllCreditCardActivities(String ccNo){
        CreditCard creditCard = this.creditCardService.getCreditCardByCcNo(ccNo);
        return new ArrayList<>(this.creditCardMovementRepository.findByCreditCard(creditCard));
    }

    List<CreditCardMovement> getMonthlyCreditCardActivities(String ccNo, int month, int year){
        CreditCard creditCard = this.creditCardService.getCreditCardByCcNo(ccNo);
        return new ArrayList<>(this.creditCardMovementRepository.findByCreditCardAndMonthAndYear(creditCard, month, year));
    }

    float monthlyCreditCardTotal(String ccNo, int month, int year){
        List<CreditCardMovement> monthlyCreditCardMovements = this.getMonthlyCreditCardActivities(ccNo, month, year);

        if (monthlyCreditCardMovements.size() < 1) return 0;

        else{
            float total = 0;
            for (CreditCardMovement cCMovement : monthlyCreditCardMovements) {
                total += cCMovement.getAmount();
            }
            return total;
        }
    }

    public CreditCardMonthlyMovements getCreditCardMonthlyMovements(String ccNo, int month, int year){
        List<CreditCardMovement> monthlyCreditCardMovements = this.getMonthlyCreditCardActivities(ccNo, month, year);
        float totalCreditCardAmount = this.monthlyCreditCardTotal(ccNo, month, year);
        return new CreditCardMonthlyMovements(monthlyCreditCardMovements, totalCreditCardAmount);
    }

    /* come faccio a fare il report mensile?
       - i movimenti sul conti ed il loro totale <--- due richieste contemporanee!!! forse una!
       i movimenti sulle carte associate al conto ed il loro totale
     */


    /**
     * *** MODIFIERS ******
     */

    private void saveCreditCardMovement(CreditCardMovement creditCardMovement, float amount){

        this.creditCardMovementRepository.save(creditCardMovement);

        //update account balance
        Account associatedAcc = creditCardMovement.getCreditCard().getAccount();
        associatedAcc.setAvailAmount(associatedAcc.getAvailAmount() - amount);
        this.accountService.updateAccountBalance(associatedAcc);
    }


    void uploadCreditCardMovements(File csvFile){
        List<CreditCardMovementUtils> cCardMvmtList = new ArrayList<>(ParseFiles.getCCMovementsFromCsv(csvFile));
        for (CreditCardMovementUtils cCardMvmt : cCardMvmtList) {
            int year = cCardMvmt.getYear();
            int month = cCardMvmt.getMonth();
            int day = cCardMvmt.getDay();
            float amount = cCardMvmt.getAmount();

            // validate parameters
            ValidationUtils.checkDateValidity(year, month,day);
            ValidationUtils.checkAmount(amount);

            CreditCard creditCard = this.creditCardService.getCreditCardByCcNo(cCardMvmt.getcCNo().toUpperCase());
            CreditCardMovement creditCardMovement = new CreditCardMovement(creditCard, day, month, year, amount);
            this.saveCreditCardMovement(creditCardMovement, amount);
        }
    }
}
