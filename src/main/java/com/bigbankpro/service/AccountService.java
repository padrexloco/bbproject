package com.bigbankpro.service;

import com.bigbankpro.model.Account;
import com.bigbankpro.exception.NoCoherentRequestException;
import com.bigbankpro.exception.NoSuchElementIdException;
import com.bigbankpro.model.CreditCard;
import com.bigbankpro.repository.AccountRepository;
import com.bigbankpro.utils.ParseFiles;
import com.bigbankpro.utils.entityUtils.AccountCreditCardUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Padre Loco on 12/11/2019.
 * Copyright (c) 2019 All rights reserved.
 */

@Service
//@RequiredArgsConstructor
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private CreditCardService creditCardService;


    /**
     * ****** GETTERS *********
     */

    Account getAccountByAccNo(String accNo){
        accNo = accNo.toUpperCase();
        Optional<Account> accountOptional = this.accountRepository.findByAccNo(accNo);
        if(!accountOptional.isPresent()) throw new NoSuchElementIdException("No account ", accNo);
        return accountOptional.get();
    }

    public Boolean checkWhetherAccountExists(String accNo){
        accNo = accNo.toUpperCase();
        Optional<Account> accountOptional = this.accountRepository.findByAccNo(accNo);
        return !accountOptional.isPresent();
    }


    /**
     * ******  MODIFIERS *********
     */

    public void insertNewAccount(String accNo){
        accNo = accNo.toUpperCase();
        Optional<Account> accountOptional = this.accountRepository.findByAccNo(accNo);
        if(accountOptional.isPresent()) throw new NoCoherentRequestException("The account number ", accNo);
        Account newAccount = new Account(accNo);
        this.accountRepository.save(newAccount);
    }

    // As consequence to account movement.
    void updateAccountBalance(Account account){
        this.accountRepository.save(account);
    }


    synchronized void uploadAccountAndAssociatedCreditCard(File file){
        AccountCreditCardUtils accountCreditCardUtils =  ParseFiles.getAccountsAndCreditCardsFromXml(file);
        List<Account> newAccounts = accountCreditCardUtils.getAccounts();
        List<CreditCard> newCreditCards = accountCreditCardUtils.getCreditCards();
        List<Account> emptyAccountList = new ArrayList<>();
        List<CreditCard> emptyCreditCards = new ArrayList<>();

        saveNewAccountsOrCreditCards(newAccounts,emptyCreditCards );
        saveNewAccountsOrCreditCards(emptyAccountList, newCreditCards);

    }

    private void saveNewAccountsOrCreditCards(List<Account> newAccounts, List<CreditCard> newCreditCards){
        if (newAccounts.size() > 0){ newAccounts.forEach(this::saveAccount);
        }else newCreditCards.forEach(this::saveCreditCard);
    }

    public Account saveAccount(Account account){
        return this.accountRepository.save(account);
    }

    public void saveCreditCard(CreditCard creditCard){
        this.creditCardService.saveACreditCard(creditCard);
    }
}
