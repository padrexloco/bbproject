package com.bigbankpro.model;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Padre Loco on 11/11/2019.
 * Copyright (c) 2019 All rights reserved.
 */

@Entity
public class CreditCardMovement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ccMovementId;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "ccNo")
    private CreditCard creditCard;

    @NotNull
    @Column(updatable = false)
    private int day;
    @NotNull
    @Column(updatable = false)
    private int month;
    @NotNull
    @Column(updatable = false)
    private int year;

    @NotNull
    private float Amount;

    public CreditCardMovement() {
    }

    public CreditCardMovement(@NotNull CreditCard creditCard,
                              @NotNull int day,
                              @NotNull int month,
                              @NotNull int year,
                              @NotNull float amount)
    {
        this.creditCard = creditCard;
        this.day = day;
        this.month = month;
        this.year = year;
        Amount = amount;
    }

    public Long getCcMovementId() {
        return ccMovementId;
    }

    public void setCcMovementId(Long ccMovementId) {
        this.ccMovementId = ccMovementId;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public float getAmount() {
        return Amount;
    }

    public void setAmount(float amount) {
        Amount = amount;
    }
}
