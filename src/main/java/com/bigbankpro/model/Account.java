package com.bigbankpro.model;

import javax.persistence.Entity;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Padre Loco on 11/11/2019.
 * Copyright (c) 2019 All rights reserved.
 */

@Entity
public class Account {

    @Id
    @Size(min = 10, max = 10) //exactly 10!
    private String accNo;

    @NotNull
    private float availAmount;

    public Account() {
    }

    public Account(String accNo) {
        this.availAmount = 0;
        this.accNo = accNo;
    }

    public Account(String accNo, @NotNull float availAmount) {
        this.accNo = accNo;
        this.availAmount = availAmount;
    }

    public String getAccNo() {
        return accNo;
    }

    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }

    public float getAvailAmount() {
        return availAmount;
    }

    public void setAvailAmount(float availAmount) {
        this.availAmount = availAmount;
    }

    @Override
    public String toString() {
        return "Account{" +
                "Account Number: " + accNo +
                ", Balance available = '" + availAmount +
                '}';
    }
}
