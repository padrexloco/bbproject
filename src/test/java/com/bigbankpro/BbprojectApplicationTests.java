package com.bigbankpro;

import com.bigbankpro.model.Account;
import com.bigbankpro.model.CreditCard;
import com.bigbankpro.service.AccountActivityService;
import com.bigbankpro.service.AccountService;
import com.bigbankpro.service.CreditCardMovementService;
import com.bigbankpro.service.CreditCardService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class BbprojectApplicationTests {

	@Autowired
	CreditCardService creditCardService;

	@Autowired
	CreditCardMovementService cardMovementService;

	@Autowired
	AccountService accountService;

	@Autowired
	AccountActivityService accountActivityService;



	@Test
	void testAddAccount(){

	}

	@Test
	void testAddCreditCard(){

	}

	@Test
	void testDebitAccountActivity() {

	}

	@Test
	void testCreditAccountActivity(){

	}

	@Test
	void testAddCreditCardMovement(){

	}

	@Test
	void contextLoads() {
	}

}
