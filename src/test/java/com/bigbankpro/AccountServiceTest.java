package com.bigbankpro;

import com.bigbankpro.model.Account;
import com.bigbankpro.model.CreditCard;
import com.bigbankpro.repository.AccountRepository;
import com.bigbankpro.service.AccountService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class AccountServiceTest {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private AccountService accountService;

    String accNo1Esistente = "CC07800654";
    String accNo2Esistente ="CC78000089";
    String accNoNonEsistente ="CC78000000"; // non esiste ancora!

    Account account1 = new Account(accNo1Esistente, 234.78f);
    Account account2 = new Account(accNo2Esistente, 67f);


    @Test
    void saveNewAccountSavedCorrectly(Account account1){

        assert(accountService.saveAccount(account1)) != null;
    }

    @Test
    void saveNewCreditCardTest(Account account){
        String cCNo1Esistente = "EXS896"; //=> associata al conto account
        String cCNo2Esistente = "VEH897"; //=> associata al conto account2
        String cCNo3Esistente = "EX46TS"; //=> associata al conto account1
        String cCNoNoEsistente = "EX46TS";



        CreditCard creditCard = new CreditCard("EXS896", account);
        CreditCard creditCard2 = new CreditCard("VEH897", account2);
        CreditCard creditCard3= new CreditCard("EX46TS", account);
    }
}